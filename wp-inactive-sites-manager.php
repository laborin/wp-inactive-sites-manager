<?php
/*
Plugin Name: WP Inactive Sites Manager
Plugin URI: http://elaborin.com/
Description: This plugin adds a useful settings page with optional and configurable automated actions to perform when a site goes inactive in your network.
Version: 0.2
Author: Emmanuel Laborin
Author URI: http://elaborin.com
License: GPLv2
Network: True
*/

defined( 'ABSPATH' ) or die( 'Plugin file cannot be accessed directly.' );

include_once( plugin_dir_path( __FILE__ ) . '/class-inactive-sites-table.php' );

if ( ! class_exists( 'WP_Inactive_Sites_Manager' ) ) {
	class WP_Inactive_Sites_Manager {

		/**
		 * Tag identifier used by file includes and selector attributes.
		 * @var string
		 */
		protected $tag = 'wism';

		/**
		 * User friendly name used to identify the plugin.
		 * @var string
		 */
		protected $name = 'WP Inactive Sites Manager';

		/**
		 * Current version of the plugin.
		 * @var string
		 */
		protected $version = '0.1';


		/**
		 * Initiate the plugin by setting the default values and assigning any
		 * required actions and filters.
		 *
		 * @access public
		 */
		public function __construct() {

			if ( is_network_admin() ) {
				add_action( 'network_admin_menu', array( &$this, 'the_menu' ) );
				add_action( 'network_admin_edit_'.$this->tag.'_the_settings_page', array( &$this, 'the_settings_page' ) );
				add_action( $this->tag.'daily_check', array( &$this, 'update_unactives' ) );
				$schedule = wp_get_schedule( $this->tag.'daily_check' );
				$page = $_GET['page'];
				
				// Unconfirmed changes deactivates the scheduled WP CRON task, so lets remain the administrator to confirm
				if( $schedule === false && $page != $this->tag."_the_settings_page" && $page != $this->tag."_the_confirmation_page" && ! isset( $_GET["plugin_status"] ) ) {
					add_action( 'network_admin_notices', array( &$this, 'unconfirmed_settings_notice' ) );
				}
			} else if ( is_admin() ) {
				$owners_blog_id = get_current_blog_id();
				global $wpdb;
				$table_name = $wpdb->base_prefix.$this->tag.'inactive';
				$result = $wpdb->get_var( "SELECT COUNT(*) FROM {$table_name} WHERE blog_id = {$owners_blog_id}" );
				if($result != 0) add_action( 'admin_notices', array( &$this, 'owner_warning_notice' ) );
			}
			add_action('plugins_loaded', array( &$this, 'load_textdomain' ) );
			add_action( 'delete_blog', array( &$this, 'blog_deleted' ), 10, 2 );
			add_action ( 'wpmu_blog_updated', array( &$this, 'blog_updated' ), 10, 1 );
			register_activation_hook( __FILE__, array( &$this, 'plugin_activation' ) );
			register_deactivation_hook( __FILE__, array( &$this, 'plugin_deactivation' ) );
		}

		/**
		 * Loads the text domain for internationalization
		 *
		 */
		public function load_textdomain() {
			$loaded = load_plugin_textdomain( 'wp-inactive-sites-manager', false, plugin_basename( dirname(__FILE__) ) . '/languages/' );
			$this->name = _x( 'WP Inactive Sites Manager', 'Plugin friendly name', 'wp-inactive-sites-manager' );
		}

		/**
		 * Fired when a blog is deleted. We must delete its record from our inactives table
		 *
		 * @param int $blog_id Blog ID
		 * @param bool $drop True if blog's table should be dropped. Default is false.
		 */
		public function blog_deleted( $blog_id, $drop ) 
		{
			global $wpdb;
			$table_name = $wpdb->base_prefix.$this->tag.'inactive';
			$wpdb->query( "DELETE FROM {$table_name} WHERE blog_id = {$blog_id}" );
		}

		/**
		 * Fired when a blog is updated. Whe must delete its record from our inactives table
		 *
		 * @param int $blog_id Blog ID
		 * 
		 */
		public function blog_updated( $blog_id ) 
		{
			global $wpdb;
			$table_name = $wpdb->base_prefix.$this->tag.'inactive';
			$wpdb->query( "DELETE FROM {$table_name} WHERE blog_id = {$blog_id}" );
		}

		/**
		 * Renders the unconfirmed settings notice to network administrators
		 *
		 * @access public
		 */
		public function unconfirmed_settings_notice() {
			$class = "update-nag";
			$message = sprintf( _x( 'You have unconfirmed settings for the %s plugin. The plugin won\'t work automatically until you save and confirm the settings.','Unconfirmed settings notice for the network administrator','wp-inactive-sites-manager' ), $this->name );
			echo"<div class=\"$class\"> <p>$message</p></div>"; 
		}

		/**
		 * Renders the inactive warning to site administrators
		 *
		 * @access public
		 */
		public function owner_warning_notice() {
			$class = "update-nag";
			$message = _x( 'We have not seen an update in your site for a while and we had to mark it as inactive in our database and will soon be removed. Please update your site as soon as possible so we can unmark it as inactive.','Warning admin notice for site owners','wp-inactive-sites-manager' );
			echo"<div class=\"$class\"> <p>$message</p></div>"; 
		}

		/**
		 * Handles the cancellation of the plugin activation if it is being performed on a non-multisite installation
		 *
		 * @access public
		 */
		public function plugin_activation( $network_wide ) {
			if ( ! is_multisite() ) {
				deactivate_plugins( plugin_basename( __FILE__ ), TRUE, TRUE );
				wp_die( sprintf( _x( 'The %s plugin is designed to be activated on multisite environments only.','Error message for non-multisite administrators who tries to activate the plugin','wp-inactive-sites-manager' ), $this->name ) );
			}

			global $wpdb;

			$charset_collate = '';
			if ( !empty ( $wpdb->charset ) ) $charset_collate = "DEFAULT CHARACTER SET {$wpdb->charset}";
			if ( !empty ( $wpdb->collate ) ) $charset_collate .= " COLLATE {$wpdb->collate}";

			$table_name = $wpdb->base_prefix.$this->tag.'inactive';

			$sql = "CREATE TABLE IF NOT EXISTS {$table_name} (
				id bigint(20) NOT NULL AUTO_INCREMENT,
				blog_id bigint(20) NOT NULL DEFAULT 0,
				inactive_since datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
				action_performed int NOT NULL DEFAULT 1,
				UNIQUE KEY id (id)
			) {$charset_collate};";
	
			require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
			dbDelta( $sql );
		}

		/**
		 * Handles the plugin deactivation
		 *
		 * @access public
		 */
		public function plugin_deactivation( ) {
			// Remove the wp cron task
			wp_clear_scheduled_hook( $this->tag.'daily_check' );
			// Delete the settings
			delete_option( $this->tag.'_settings' );
			// Drop the plugin table
			global $wpdb;
			$table_name = $wpdb->base_prefix.$this->tag.'inactive';
			$wpdb->query( "DROP TABLE IF EXISTS {$table_name}" );
		}

		/**
		 * Initializes the plugin settings
		 *
		 * @access public
		 */
		public function init_settings() {
			add_settings_section(
				$this->tag.'_settings', 
				_x( 'Settings', 'Title for the settings section' ,'wp-inactive-sites-manager' ),
				false, 
				$this->tag.'_the_settings_page'
			);

			register_setting( $this->tag.'_the_settings_page', $this->tag.'_settings' );

			add_settings_field( 
				$this->tag.'_days_to_inactive', 
				_x( 'Days to inactive', 'days to inactive field title', 'wp-inactive-sites-manager' ),
				array( &$this, 'days_to_inactive_field_renderer' ), 
				$this->tag.'_the_settings_page', 
				$this->tag.'_settings' 
			);

			add_settings_field( 
				$this->tag.'_days_to_execute',
				_x( 'Days to execute', 'days to execute field title', 'wp-inactive-sites-manager' ),
				array( &$this, 'days_to_execute_field_renderer' ), 
				$this->tag.'_the_settings_page', 
				$this->tag.'_settings' 
			);

			add_settings_field( 
				$this->tag.'_action', 
				_x( 'Action', 'action field title', 'wp-inactive-sites-manager' ),
				array( &$this, 'action_field_renderer' ), 
				$this->tag.'_the_settings_page', 
				$this->tag.'_settings' 
			);

			add_settings_field( 
				$this->tag.'_email_body',
				_x( 'Email body', 'email body field title', 'wp-inactive-sites-manager' ),
				array( &$this, 'email_body_field_renderer' ), 
				$this->tag.'_the_settings_page', 
				$this->tag.'_settings' 
			);

			// Set the default values
			$currentValues = get_option( $this->tag.'_settings' );
			if( false === $currentValues ) {
				update_option($this->tag."_settings", array(
						$this->tag."_days_to_inactive" => 60,
						$this->tag."_days_to_execute" => 30,
						$this->tag."_action" => 1,
						$this->tag."_email_body" => _x( 'Dear site owner. We have not seen an update in your site for a while and we had to mark it as inactive in our database and will soon be removed. Please update your site as soon as possible so we can unmark it as inactive.', 'Default value for the email_body field','wp-inactive-sites-manager' )
					)
				);
				// The regular way to set wp cron tasks is in the activation hook, but we need the settings set so lets do it from here
				wp_schedule_event(time(), 'daily', $this->tag.'daily_check');
			}
		}

		/**
		 * Renders the days_to_nactive settings field
		 *
		 * @access public
		 */
		public function days_to_inactive_field_renderer(  ) { 
			$options = get_option( $this->tag.'_settings' );
			?>
			<input type='number' name='<?php echo $this->tag; ?>_settings[<?php echo $this->tag; ?>_days_to_inactive]' value='<?php echo $options[$this->tag.'_days_to_inactive']; ?>'>
			<p class="description"><?php _ex( 'Amount of days a site should be idle to be considered as inactive', 'Description for the days_to_inactive field','wp-inactive-sites-manager' ); ?></p>
			<?php
		}

		/**
		 * Renders the days_to_execute settings field
		 *
		 * @access public
		 */
		public function days_to_execute_field_renderer(  ) { 
			$options = get_option( $this->tag.'_settings' );
			?>
			<input type='number' name='<?php echo $this->tag; ?>_settings[<?php echo $this->tag; ?>_days_to_execute]' value='<?php echo $options[$this->tag.'_days_to_execute']; ?>'>
			<p class="description"><?php _ex( 'Amount of days a site should be inactive to perform the programmed action', 'Description for the days_to_execute field','wp-inactive-sites-manager' ); ?></p>
			<?php
		}

		/**
		 * Renders the action settings field
		 *
		 * @access public
		 */
		public function action_field_renderer(  ) { 
			$options = get_option( $this->tag.'_settings' );
			?>
			<select name='<?php echo $this->tag; ?>_settings[<?php echo $this->tag; ?>_action]'>
				<option value='1' <?php selected( $options[$this->tag.'_action'], 1 ); ?>><?php _ex( 'Do nothing', 'Possible option for the action field','wp-inactive-sites-manager' ); ?></option>
				<option value='2' <?php selected( $options[$this->tag.'_action'], 2 ); ?>><?php _ex( 'Archive', 'Possible option for the action field','wp-inactive-sites-manager' ); ?></option>
				<option value='3' <?php selected( $options[$this->tag.'_action'], 3 ); ?>><?php _ex( 'Deactivate', 'Possible option for the action field','wp-inactive-sites-manager' ); ?></option>
				<option value='4' <?php selected( $options[$this->tag.'_action'], 4 ); ?>><?php _ex( 'Delete', 'Possible option for the action field','wp-inactive-sites-manager' ); ?></option>
			</select>
			<p class="description"><?php _ex( 'Action to perform on the sites that exceeds the allowed inactive period', 'Description for the action field','wp-inactive-sites-manager' ); ?></p>
			<?php
		}

		/**
		 * Renders the email_body settings field
		 *
		 * @access public
		 */
		public function email_body_field_renderer(  ) { 
			$options = get_option( $this->tag.'_settings' );
			?>
			<textarea cols='80' rows='8' name='<?php echo $this->tag; ?>_settings[<?php echo $this->tag; ?>_email_body]'><?php echo $options[$this->tag.'_email_body']; ?></textarea>
			<p class="description"><?php _ex( 'Content for the email that will be sent to site owners when they enter the inactive status', 'Description for the email_body field','wp-inactive-sites-manager' ); ?></p>
			<?php
		}

		/**
		 * Add the plugin page to the admin menu
		 *
		 * @access public
		 */
		public function the_menu() {

			add_menu_page(
				$this->name,
				_x( 'Inactive sites', 'Main menu item text','wp-inactive-sites-manager' ),
				'manage_sites',
				$this->tag.'_the_main_page',
				array( &$this, 'the_main_page' ),
				'dashicons-clock',
				'6'
			);
			add_submenu_page(
				$this->tag.'_the_main_page',
				sprintf( _x('%s settings', 'Page title for the settings page','wp-inactive-sites-manager' ), $this->name ),
				_x( 'Settings', 'Menu item text for the settings page','wp-inactive-sites-manager' ),
				'manage_sites',
				$this->tag.'_the_settings_page',
				array( &$this, 'the_settings_page' )
			);
			add_submenu_page(
				null,
				sprintf( _x('%s confirm changes', 'Page title for the confirm changes page','wp-inactive-sites-manager' ), $this->name ),
				_x( 'Confirmation', 'Menu item text for the confirmation page. This is never displayed anyways', 'wp-inactive-sites-manager' ),
				'manage_sites',
				$this->tag.'_the_confirmation_page',
				array( &$this, 'the_confirmation_page' )
			);
			$this->init_settings();
		}

		/**
		 * Checks for new inactive sites and performs the programmed action on the sites that has been inactive for too long
		 *
		 * @access public
		 */
		public function update_unactives( $clear_table = false ) {
			global $wpdb;
			$table_name = $wpdb->base_prefix.$this->tag.'inactive';
			$settings = get_option( $this->tag.'_settings' );
			$days_to_inactive = intval( $settings[$this->tag.'_days_to_inactive'] );
			$days_to_execute = intval( $settings[$this->tag.'_days_to_execute'] ) + $days_to_inactive;
			$action_to_perform = intval( $settings[$this->tag.'_action'] );
			$email_body = $settings[$this->tag.'_email_body'];

			// Only clear the table as a result of a new settings configuration
			if( $clear_table ) $wpdb->query( "TRUNCATE TABLE {$table_name}" );

			// Insert the new inactive sites, avoiding the main site of course
			$wpdb->query( "INSERT INTO {$table_name} (blog_id, inactive_since)
							SELECT st.blog_id, st.last_updated
							FROM wp_blogs st
							WHERE st.blog_id <> 1 AND st.last_updated <> '0000-00-00 00:00:00' AND st.last_updated < DATE_SUB(NOW(), INTERVAL {$days_to_inactive} DAY) AND  NOT EXISTS (
												SELECT 1 
												FROM {$table_name} t2
												WHERE t2.blog_id = st.blog_id )" );
			
			// Notify new inactive sites owners about the situation
			$new_sites = $wpdb->get_results( "SELECT * FROM {$table_name} WHERE inactive_since > DATE_SUB(NOW(), INTERVAL 1 DAY);");
			foreach ( $new_sites as $site ) {
				switch_to_blog( $site->blog_id );
				$mail = get_option('admin_email');
				restore_current_blog();
				wp_mail( $mail, _x( 'Your site is inactive', 'Title for the email sent to site owners when their site goes inactive','wp-inactive-sites-manager' ), $email_body );
			}

			// Perform the programmed action on the expired sites
			if(1 != $action_to_perform) {
				// Get the newly doomed sites
				$doomed_sites = $wpdb->get_results( "SELECT * FROM {$table_name} WHERE inactive_since < DATE_SUB(NOW(), INTERVAL {$days_to_execute} DAY) AND action_performed = 1;");
				if( $wpdb->num_rows > 0 ) {
					foreach ( $doomed_sites as $site ) 
					{
						switch($action_to_perform) {
							case 2:
								update_blog_status($site->blog_id, 'archived', 1);
								break;
							case 3:
								update_blog_status($site->blog_id, 'deleted', 1);
								break;
							case 4:
								wpmu_delete_blog( $site->blog_id, true );
								break;
						}
						// Don't worry, deleted sites will be automatically deleted from out inactives table via hook
						$wpdb->query( "UPDATE {$table_name} SET action_performed = $action_to_perform WHERE blog_id = {$site->blog_id}" );
					}
				}
			}
			
		}

		/**
		 * Counts the amount of blogs that will be deleted with an hypothetical set of settings
		 *
		 * @access private
		 */
		private function blogs_to_be_deleted( $settings ) {
			// Actually, only the 4th option deletes blogs
			if( intval( $settings[$this->tag.'_action'] ) != 4 ) return 0;
			global $wpdb;
			$table_name = $wpdb->base_prefix.$this->tag.'inactive';
			$total_days = intval( $settings[$this->tag.'_days_to_inactive'] ) + intval( $settings[$this->tag.'_days_to_execute'] );
			$resultado = $wpdb->get_var( "SELECT COUNT(*) FROM wp_blogs WHERE last_updated <> '0000-00-00 00:00:00' AND last_updated < DATE_SUB(NOW(), INTERVAL {$total_days} DAY)" );
			return $resultado;
		}

		/**
		 * Renders the main plugin page
		 *
		 * @access public
		 */
		public function the_main_page() {

			if( isset( $_GET["settings_updated"] ) ) echo '<div class="updated"><p>'._x( 'Settings updated!', 'Admin notice that is shown when the settings has been successfully edited','wp-inactive-sites-manager' ).'</p></div>';
			
			// If we get a site or some sites to remove...
			$sites_to_remove = [];
			if( isset( $_GET['action'] ) && $_GET['action'] == 'unmark_inactive' ) $sites_to_remove[] = intval( $_GET['site'] );
			if( isset( $_POST[ 'action' ] ) ) {

				// Get the action to perform, the upper combo box has preference
				$action = intval( $_POST[ 'action' ] ) == -1 ? ( intval( $_POST[ 'action2' ] ) == -1 ? FALSE : $_POST[ 'action2' ] ) : $_POST[ 'action' ];
				if( $action === 'unmark_inactive' && isset( $_POST['site'] ) && count( $_POST['site'] ) > 0 ) {
					foreach ($_POST['site'] as $site) {
						$sites_to_remove[] = intval($site);
					}
				}

			}

			// Just update the last_updated timestamp and fire wpmu_blog_update so our hook deletes it automatically from our inactives table
			foreach ($sites_to_remove as $site) {
				update_blog_details( $site, array( 'last_updated' => current_time( 'mysql', true ) ) );
				do_action( 'wpmu_blog_updated', $site );
			}

			if( count( $sites_to_remove ) > 0 ) {
				echo '<div class="updated"><p>'.sprintf( _nx( '%s site removed from our inactives list', '%s sites removed from our inactives list', count( $sites_to_remove ), 'Admin notice to inform that a site has been removed from the inactives list', 'wp-inactive-sites-manager' ), count( $sites_to_remove ) ).'</p></div>';
			}

			// Prepare the table and render our main page
			global $wpdb;
			$table_name = $wpdb->base_prefix.$this->tag.'inactive';
			$table = new Inactive_Sites_Table();
			$table->prepare_items( $wpdb->get_results( "SELECT * FROM {$table_name}", ARRAY_A ) );
			?>
			<div class="wrap">
				<div id="icon-users" class="icon32"></div>
				<h2><?php _ex( 'Currently Inactive Sites', 'Header text for the main page','wp-inactive-sites-manager' ); ?></h2>
				<form method="POST">
					<?php $table->display(); ?>
				</form>
			</div>
			<?php
		}

		/**
		 * Processes and renders the settings page
		 *
		 * @access public
		 */
		public function the_settings_page() {
			
			// If we have configuration data, let's process it
			if ( isset( $_POST[$this->tag."_settings"] ) ) {

				// Remove the wp cron job
				wp_clear_scheduled_hook( $this->tag.'daily_check' );

				// Will be saved as an option for the base blog, seems like a good place to store network-wide plugin stuff. Some sanitization here would be nice
				update_option($this->tag."_settings", $_POST[$this->tag."_settings"]);

				// Only redirect the user to the confirmation page if there is at least 1 blog set to deletion with the new settings
				$blogs_to_be_deleted = $this->blogs_to_be_deleted( $_POST[$this->tag."_settings"] );
				if($blogs_to_be_deleted > 0) {

					wp_redirect(add_query_arg(array('page' => $this->tag.'_the_confirmation_page', 'to_delete' => $blogs_to_be_deleted), network_admin_url('admin.php')));

				// Ok, no risk of losing data by mistake, let's apply tha changes immediately then, without asking for confirmation
				} else {

					// Update our table, telling the function to clear our current table
					$this->update_unactives(true);

					// Schedule to run daily, but as we have just run the process, the first scheduled run will be 86400 seconds (a day) in the future
					wp_schedule_event(time() + 86400, 'daily', $this->tag.'daily_check');
					wp_redirect(add_query_arg(array('page' => $this->tag.'_the_main_page', 'settings_updated' => 'true'), network_admin_url('admin.php')));

				}

			// Else, maybe we have a confirmation response
			}else if( isset( $_POST["confirmation"] ) ) {

				// If the response is not a "NO" (hint: it's a YES)
				if( strpos( strtoupper( $_POST["confirmation"] ), 'NO' ) === FALSE ) {

					// Update our table, telling the function to clear our current table
					$this->update_unactives(true);

					// Schedule to run daily, but as we have just run the process, the first scheduled run will be 86400 seconds (a day) in the future
					wp_schedule_event(time() + 86400, 'daily', $this->tag.'daily_check');
					wp_redirect(add_query_arg(array('page' => $this->tag.'_the_main_page', 'settings_updated' => 'true'), network_admin_url('admin.php')));

				// If the response was a NO, then redirect him to the settings page so he can re-thin
				}else {

					wp_redirect(add_query_arg(array('page' => $this->tag.'_the_settings_page'), network_admin_url('admin.php')));

				}

			}
			
			// At this poing, we didn't found any POST data of our interest, so let's render the regular settings page
			?>
			<div class="wrap">
				<h1><?php echo $this->name; ?></h1>
				<form action='edit.php?action=<?php echo $this->tag; ?>_the_settings_page' method='post'>
					<?php
						settings_fields( $this->tag.'_the_settings_page' );
						do_settings_sections( $this->tag.'_the_settings_page' );
						submit_button();
					?>
				</form>
			</div>
			<?php
		}

		/**
		 * Renders the confirmation page. I'll use this page to warn the administator about the sites being deleted with the new configuration, giving a chance to cancel everything
		 *
		 * @access public
		 */
		public function the_confirmation_page() {
			?>
			<div class="error">
				<p><?php printf( _x( 'Warning: With the selected settings, %s sites <strong>will be permanently deleted</strong>', 'Warning notice shown in the confirmation page','wp-inactive-sites-manager' ), $_GET['to_delete'] ); ?></p>
			</div>
			<div class="wrap">
				<h1><?php printf( _x( '%s - Confirm changes', 'Header shown in the confirmation page','wp-inactive-sites-manager' ), $this->name ); ?></h1>
				<form action='edit.php?action=<?php echo $this->tag; ?>_the_settings_page' method='post'>
					<p><?php _ex('Are you sure you want to apply the changes?', 'Confirmation text', 'wp-inactive-sites-manager'); ?></p>
					<button name="confirmation" value="yes" type="submit"><?php _ex('Yes, delete everything', 'Confirmation option', 'wp-inactive-sites-manager'); ?></button>
					<button name="confirmation" value="no" type="submit"><?php _ex('NO WAY, let me check these settings again', 'Confirmation option', 'wp-inactive-sites-manager'); ?></button>
				</form>
			</div>
			<?php
		}

	}
	new WP_Inactive_Sites_Manager;
}
