=== WP Inactive Sites Manager ===
Tags: multisite, inactive, admin, network
Requires at least: 4.4.1
Tested up to: 4.4.1
Stable tag: 0.2
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

This plugin adds a useful settings page with optional and configurable automated actions to perform when a site goes inactive in your network.

== Description ==

This plugin adds a useful settings page with optional and configurable automated actions to perform when a site goes inactive in your network.

It does it by allowing the administrator to set the amount of days a site must stay without being updated to be considered inactive. Then, after a (also configurable) amount of time being inactive, the site will be automatically kicked out (configurable to deactivation, archive or permanently deletion). The plugin warns all site owners by email and inside his dashboard when his site enters the inactive umbral.

== Technical explanation ==

* The plugin can be activated only on multisite wordpress installations. On regular WP sites it won't activate.
* At activation, a table and a cron task are created, along with a set of non destructive default settings.
* The main plugin page shows a list of sites the plugin considers inactive.
* For a site to be considered inactive, its last_updated timestamp should be older than the amount of days configured in the settings page.
* The programmed action is executed on all sites that remains inactive for the amount of days set in the "wism_days_for_execute" setting field.
* If the network administrator sets a destructive set of settings that are going to delete sites, the cron task is disabled and these new settings must be confirmed in order to re activate the cron task.
* An email is sent to all site owners whose site enters the configured inactive umbral.
* A message appears on the administration area of the site owner to remain him about the situation.
* When a site gets updated, it is immediately removed from our inactives table.
* The inactives table is used basically as a cache, to show the network administrator a table with the inactive sites without performing a full query over the wp_blogs table.

== Installation ==

1. Upload the plugin folder to the `/wp-content/plugins/` directory of your wordpress multisite installation
2. Activate the plugin through the 'Plugins' menu
3. Go to the plugin settings page and configure the timeframes and the action to be taken after a site exceeds the inactive umbral.

== Changelog ==

= 0.2 =
First working version.

= 0.1 =
Work in progress, not usable yet.