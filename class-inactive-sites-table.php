<?php
defined( 'ABSPATH' ) or die( 'Plugin file cannot be accessed directly.' );
// WP_List_Table is not loaded automatically so we need to load it in our application
if( ! class_exists( 'WP_List_Table' ) ) {
	require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}
/**
 * Create a new table class that will extend the WP_List_Table
 */
class Inactive_Sites_Table extends WP_List_Table {
	/**
	 * Prepare the items for the table to process
	 *
	 * @return Void
	 */
	public function prepare_items($sites) {
		$columns = $this->get_columns();
		$hidden = $this->get_hidden_columns();
		$sortable = $this->get_sortable_columns();
		$this->_column_headers = array($columns, $hidden, $sortable);
		$this->items = $sites;
	}

	/**
	 * Override the parent columns method. Defines the columns to use in your listing table
	 *
	 * @return Array
	 */
	public function get_columns() {
		$columns = array(
			'cb' => '<input type="checkbox" />',
			'blog_id'       => _x( 'Site', 'Column header', 'wp-inactive-sites-manager' ),
			'inactive_since' => _x( 'Inactive since', 'Column header', 'wp-inactive-sites-manager' ),
			'action_performed'        => _x( 'Action performed', 'Column header', 'wp-inactive-sites-manager' )
		);
		return $columns;
	}

	/**
	 * Set the bulk actions for the table
	 *
	 * @return Array
	 */
	public function get_bulk_actions() {
		$actions = array(
			'unmark_inactive'    => _x( 'Remove from inactives', 'Table bulk actions option','wp-inactive-sites-manager' )
		);
		return $actions;
	}

	/**
	 * Renders the site column cell, all with the options
	 *
	 * @return String
	 */
	public function column_blog_id( $item ) {
		$actions = array(
			'edit' => sprintf('<a href="?page=%s&action=%s&site=%s">'._x( 'Remove from inactives', 'Table option','wp-inactive-sites-manager' ).'</a>',$_REQUEST['page'],'unmark_inactive',$item['blog_id'])
		);
		$blog_details = get_blog_details( $item[ 'blog_id' ] );
		$column_text = $blog_details->domain.$blog_details->path;
		$column_actions = $this->row_actions($actions);

		return sprintf('%1$s %2$s', $column_text, $column_actions );
	}

	/**
	 * Renders the checkbox column cell
	 *
	 * @return String
	 */
	public function column_cb( $item ) {
		return '<input type="checkbox" name="site[]" value="'.$item[ 'blog_id' ].'" />';
	}

	/**
	 * Define which columns are hidden
	 *
	 * @return Array
	 */
	public function get_hidden_columns() {
		return array();
	}
	/**
	 * Define the sortable columns
	 *
	 * @return Array
	 */
	public function get_sortable_columns() {
		return array('action_performed' => array('action_performed', false));
	}
	
	/**
	 * Define what data to show on each column of the table
	 *
	 * @param  Array $item        Data
	 * @param  String $column_name - Current column name
	 *
	 * @return Mixed
	 */
	public function column_default( $item, $column_name ) {
		switch( $column_name ) {
			case 'action_performed': {
				switch( intval($item[ $column_name ] ) ) {
					case 1:
						return _x( 'None... yet?', 'Possible action taken', 'wp-inactive-sites-manager' );
						break;
					case 1:
						return _x( 'Archived', 'Possible action taken', 'wp-inactive-sites-manager' );
						break;
					case 1:
						return _x( 'Deactivated', 'Possible action taken', 'wp-inactive-sites-manager' );
						break;
					case 1:
						return _x( 'Deleted', 'Possible action taken', 'wp-inactive-sites-manager' ); // This wont be shown, but let's code the case
						break;
				}
				break;
			}
			default:
				return $item[ $column_name ];
		}
	}
	/**
	 * Allows you to sort the data by the variables set in the $_GET
	 *
	 * @return Mixed
	 */
	private function sort_data( $a, $b ) {
		// Set defaults
		$orderby = 'title';
		$order = 'asc';
		// If orderby is set, use this as the sort column
		if(!empty($_GET['orderby']))
		{
			$orderby = $_GET['orderby'];
		}
		// If order is set use this as the order
		if(!empty($_GET['order']))
		{
			$order = $_GET['order'];
		}
		$result = strcmp( $a[$orderby], $b[$orderby] );
		if($order === 'asc')
		{
			return $result;
		}
		return -$result;
	}
}